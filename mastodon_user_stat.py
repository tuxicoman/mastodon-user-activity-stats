from mastodon import Mastodon
import time
import matplotlib.pyplot as plt

first_time = False
target_user = "tuxicoman@social.jesuislibre.net" #example

if first_time:
  instance = "" #FILL THIS the domain of your instance
  user = "" #FILL THIS with a valid user login
  password = "" #FILL THIS with a valid user login

  print("Registering application...")
  api_base_url = "https://" + instance
  client_id, client_secret = Mastodon.create_app('user_activity_stat', api_base_url=api_base_url)
  access_token = Mastodon(api_base_url=api_base_url, client_id=client_id, client_secret=client_secret).log_in(username=user, password=password)
  with open("mastodo_user_stat_token.txt", "w") as f:
    f.write(api_base_url)
    f.write("\n")
    f.write(client_id)
    f.write("\n")
    f.write(client_secret)
    f.write("\n")
    f.write(access_token)
    f.write("\n")
else:
  print("Reusing application...")
  with open("mastodo_user_stat_token.txt", "r") as f:
    lines = f.readlines()
    lines = [line[:-1] for line in lines]
  api_base_url, client_id, client_secret, access_token = lines
print("... done")

mstdn = Mastodon(api_base_url=api_base_url, client_id=client_id, client_secret=client_secret, access_token=access_token )

#Find user id
user_id =  mstdn.search(target_user)["accounts"][0]["id"]

#hours table
hours = [0] * 24

#Get toots
max_id = None
max_nb_toots = 2000
nb_toots = max_nb_toots
first_toot_time = None
last_toot_time = None
while True:
  toots = mstdn.account_statuses(id=user_id, max_id=max_id)
  print("Remaining toots to collect... %i " % nb_toots)
  if len(toots) == 0:
    break
  for toot in toots:
    toot_time = time.strptime(toot["created_at"][:-5], "%Y-%m-%dT%H:%M:%S")
    if first_toot_time is None:
      first_toot_time = toot_time
    last_toot_time = toot_time
    hours[toot_time.tm_hour] += 1
    nb_toots -=1
    if nb_toots == 0:
      break
  if nb_toots == 0:
    break
  max_id = toots[-1]["id"]

nb_toots_collected = max_nb_toots - nb_toots

delta = (time.mktime(first_toot_time)-time.mktime(last_toot_time))/3600
hours = [hour/delta for hour in hours]

delta_days = delta/24

plt.bar(range(24), hours)
plt.title("%s toots per hour\nData : %i toots over %i days" % (target_user, nb_toots_collected, delta_days))
plt.show()
