# Mastodon user activity stats

Displays the user activity by scraping public posts from a user and looking at which hour they were posted.

Example : https://tuxicoman.jesuislibre.net/2019/03/graphe-dactivite-sur-mastodon.html

## Usage

First run with "first_time=True" to register the app to your mastodon account and create an access token on disk

Then use "first_time=False" as the app will reuse the token to access to the Mastodon server.